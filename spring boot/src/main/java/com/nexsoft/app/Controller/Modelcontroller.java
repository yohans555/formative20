package com.nexsoft.app.Controller;

import java.util.ArrayList;
import com.nexsoft.app.model.Model;
import com.nexsoft.app.service.Modelservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class Modelcontroller {
    @Autowired
    Modelservice modelservice;

    @GetMapping("/home")
    public ModelAndView home(){
        ModelAndView mv = new ModelAndView("index.jsp");
        mv.addObject("list", modelservice.get());
        return mv;
    }

    @RequestMapping(value ="/save", method = RequestMethod.POST)
    public void add(@RequestBody ArrayList<Model> model ){
        modelservice.saveall(model);
    }
}
