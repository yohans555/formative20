package com.nexsoft.app.model;

import javax.persistence.*;



@Entity
@Table(name="model")
public class Model {
    @Id
    private int id;
    private int postId;
    private String name,email,body;

    public Model(){}

    

    public Model(int id, int postId, String name, String email) {
        this.id = id;
        this.postId = postId;
        this.name = name;
        this.email = email;
    }

    public String getBody() {
        return body;
    }



    public void setBody(String body) {
        this.body = body;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    
    
    
}
